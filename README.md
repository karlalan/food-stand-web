# food-stand-web

## How to start server with Vagrant

```bash
cd /path/to/food-stand
vagrant up
vagrant ssh
cd /vagrant
npx ng serve --host 0.0.0.0
```
