import { Component, OnInit } from '@angular/core';

import { ApiUserService } from '../../api/user/services';
import { User } from '../../api/user/models';

@Component({
	selector: 'app-about',
	templateUrl: './about.component.html',
	styleUrls: [
		'./about.component.scss',
		'../../../static/css/about/carousel.css'
	]
})
export class AboutComponent implements OnInit {
	private users: User[];
	
	constructor(
		private apiUserService: ApiUserService
	) { }

	ngOnInit(){}

	/**
	 * Get and set information
	 * @return none
	 */
	private getUsers(): void
	{
		this.apiUserService.getAllUsers()
			.then(allUsers => this.users = allUsers);
	}

	
}
