import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: [
		'./home.component.scss',
		'../../../static/css/home/pricing.css'
	]
})
export class HomeComponent implements OnInit {

	constructor() { }

	ngOnInit()
	{
		this.initCalcQuantity();
	}

	private initCalcQuantity(): void
	{
		$(function(){
			$('.count').prop('disabled', true);
			$('.plus').on('click', function(){
				let neborInput = $(this).prev('input');
				let count: string = <string>neborInput.val();
				neborInput.val(parseInt(count) + 1 );
			});
			$('.minus').on('click', function(){
				let neborInput = $(this).next('input');
				let count: string = <string>neborInput.val();
				neborInput.val(parseInt(count) - 1 );
				if (neborInput.val() < 0) {
					neborInput.val(0);
				}
			});
		});
	}

}
