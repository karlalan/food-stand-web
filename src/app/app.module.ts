import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { AboutComponent } from './component/about/about.component';
import { ContactComponent } from './component/contact/contact.component';

import { ApiUserService } from './api/user/services';
import { HttpModule } from '@angular/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


// import ngx-translate and the http loader for i18n
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';


@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		AboutComponent,
		ContactComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpModule,
		HttpClientModule, //added for i18n
		TranslateModule.forRoot({  //added for i18n
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			}
		}),
		NgbModule
	],
	providers: [
		ApiUserService // for using PHP RESTful api
	],
	bootstrap: [
		AppComponent
	],
	exports: [
		TranslateModule,// added for i18n
	]
})
export class AppModule { }

// added for i18n
export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http);
}