import { Component } from '@angular/core';

// Added for i18n
import {  TranslateService } from '@ngx-translate/core';
import { Title } from "@angular/platform-browser";

import { Lang } from './model/lang.model';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: [
		'./app.component.scss',
		'../static/css/common/bootstrap/bootstrap.min.css',
		'../static/css/common/navbar.css'
	]
})
export class AppComponent {
	// With private accessor 'accessible' error occurs in case of build  on Heroku, so removed private.
	//     Property 'selectedLang' is private and only accessible within class 'AppComponent'
	// Wonder if it calls other problems... ?
	/* private */ navbarTitle: string = 'TITLE.SYSTEM';
	/* private */ selectedLang: string = '';
	/* private */ selectedLangName: string = '';
	/* private */ langOpts: Lang[] = [
		{'id': "en", 'displayName': 'SYSTEM.I18N.ENGLISH.ABBR'},
		{'id': "ja", 'displayName': 'SYSTEM.I18N.JAPANESE.ABBR'},
		{'id': "zh_hant", 'displayName': 'SYSTEM.I18N.CHINESE.ABBR'}
	];

	constructor (
		private translateService: TranslateService,
		private titleService: Title,
	){
		this.SetAppLang(window.navigator.language);
		this.titleService.setTitle('Food Stand');
	}

	/**
	 * Set language for app
	 * @params lang The langauge got from user's agent
	 * @return none
	 */
	private SetAppLang(lang: string): void
	{
		this.selectedLang = this.ClassifyLang(lang);
		this.ClassifyLangDisplayName(this.selectedLang);
		this.translateService.setDefaultLang(this.selectedLang);
	}

	/**
	 * Classify browser's lang to translation lang
	 * @params lang The langauge got from user's agent
	 * @return classified lang
	 */
	private ClassifyLang(lang: string): string
	{
		let lowerizedLang = lang.toLowerCase();
		return lowerizedLang.startsWith('zh') ? "zh_hant" 
			: lowerizedLang.startsWith('ja') ? "ja" : "en";
	}

	private ClassifyLangDisplayName(lang: string): void
	{
		Object.entries(this.langOpts).forEach((value) => {
			if(value[1].id === lang){
				this.selectedLangName = value[1].displayName;
				return;
			}
		})
	}

	/**
	 * Switch to selected lang
	 * @params lang The langauge got from user's agent
	 * @return none
	 */
	public SwitchLanguage(lang: string): void
	{
		this.SetAppLang(lang);
	}

}
