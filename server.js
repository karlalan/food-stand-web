/**
 * The top file to publish the project with express server.
 */

const path = require('path');
const express = require('express');

// Instantiate server.
const app = express();

// When paths are specified, return files which correspond to them statically.
// 'dist/webapp' is written in angular.json as projects.webapp.architect.build.options.outputPath.
app.use('/', express.static(`${__dirname}/dist/webapp`));

// When the root path is specified, return dist/webapp/index.html
app.get('/', (req, res) => {
    res.sendFile(path.join(`${__dirname}/dist/webapp/index.html`));
});

// Start server.
const server = app.listen(process.env.PORT || 8080, () => {
    const host = server.address().address;
    const port = server.address().port;
    console.log(`Listening at http://${host}:${port}`);
});